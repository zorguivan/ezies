var ManagerProvider = require('../provider/ManagerProvider');

module.exports = ManagerController();

function ManagerController() {

  return {
    authenticate: authenticate,
    getInfo: getInfo
  }

  function authenticate(req, res, next) {
    ManagerProvider.getUser({
      username: req.params.username,
      password: req.params.password
    }).then(function(user) {
      req.session.state = "Online";
      res.json(user);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  }

  function getInfo(req, res, next) {
    ManagerProvider.getInfo().then(function(info) {
      console.log('--------------');
      res.json(info);
    }).catch(function(err) {
      console.log(err);
      res.status(500).send(err);
    });
  }
}
