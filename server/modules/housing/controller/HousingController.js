var HousingProvider = require('../provider/HousingProvider');

module.exports = HousingController();

function HousingController() {

  return {
    addHouse: addHouse,
    savePics: savePics,
    getHouses: getHouses,
    getHouse: getHouse,
    search: search,
    GetHousePics: GetHousePics,
    updateHouse: updateHouse,
    deleteHouse: deleteHouse
  }
  console.log('Housing Controller has been called');

  function addHouse(req, res, next) {
    HousingProvider.addHouse(req.body).then(function(result) {
      res.json(result);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  }

  function savePics(req, res, next) {
    console.log('I have been called From the Housing Controller to save the house pics');
    HousingProvider.savePics(req.body).then(function(result) {
      res.json(result);
    }).catch(function(err) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      }
    });
  }

  function getHouses(req, res, next) {
    HousingProvider.getHouses().then(function(result) {
      res.json(result)
    }).catch(function(err) {
      res.status(500).send(err);
    });
  }

  function updateHouse(req, res, next) {
    HousingProvider.updateHouse(req.body).then(function(res) {
      res.json(res)
    }).catch(function(err) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      }
    })
  }

  function deleteHouse(req, res, next) {
    HousingProvider.deleteHouse(req.params.id).then(function() {
      res.send('OK');
    }).catch(function(err) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      }
    });
  }

  function getHouse(req, res, next) {
    HousingProvider.getHouse(req.params.id).then(function(result) {
      res.json(result);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  }

  function search(req, res, next) {
    HousingProvider.search(req.params[0]).then(function(result) {
      res.json(result);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  }

  function GetHousePics(req, res, next) {
    HousingProvider.getHousePics(req.params.houseId).then(function(result) {
      res.json(result);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  }

}
