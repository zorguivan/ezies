var q = require('q');

var connection;

function ManagerProvider() {

  return {
    setConnection: setConnection,
    getUser: getUser,
    getInfo: getInfo
  }

  function setConnection(conn) {
    connection = conn;
  }

console.log('Manager provider has been Loaded');

  function getUser(user) {
    var execution = q.defer();
    var query = 'SELECT * FROM users WHERE username = :username AND password = :password';
    connection.query(query, {
      username: user.username,
      password: user.password
    }, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
        return;
      }
      execution.resolve(res[0]);
    });
    return execution.promise;
  }

  function getInfo() {
    var execution = q.defer();
    var query = 'SELECT * FROM company';
    connection.query(query, function(err, res) {
      if (err) {
        console.log('-----------+---');
        console.log(err);
        execution.reject(err);
        return;
      }
      execution.resolve(res);
    });
    return execution.promise;
  }


}
module.exports = ManagerProvider();
