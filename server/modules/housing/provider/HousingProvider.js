var q = require('q');

var connection;

function HousingProvider() {

  return {
    setConnection: setConnection,
    addHouse: addHouse,
    getHouses: getHouses,
    getHouse: getHouse,
    savePics: savePics,
    getHousePics: getHousePics,
    search: search,
    updateHouse: updateHouse,
    deleteHouse: deleteHouse

  }

  function setConnection(conn) {
    connection = conn;
  }

  function search(fields) {
    var execution = q.defer();
    var query = "SELECT * FROM houses WHERE ";
    var search = fields.split("/");
    for (var i = 0; i < search.length; i += 2) {

      if (i == search.length - 2) {
        if (search[i] == 'priceMin') {
          query += "price >= " + search[i + 1];
        } else if (search[i] == 'priceMax') {
          query += "price <= " + search[i + 1];
        } else if (search[i] == 'location') {
          query += "location = '" + search[i + 1] + "'";
        } else if (search[i] == 'title') {
          query += "title = '" + search[i + 1] + "'";
        } else if (search[i] == 'adress') {
          query += "title = '" + search[i + 1] + "'";
        } else {
          query += search[i] + " = " + search[i + 1];
        }
      } else {
        if (search[i] == 'priceMin') {
          query += "price >= " + search[i + 1] + " AND ";
        } else if (search[i] == 'priceMax') {
          query += "price <= " + search[i + 1] + " AND ";
        } else if (search[i] == 'location') {
          query += "location = '" + search[i + 1] + "' AND ";
        } else if (search[i] == 'title') {
          query += "title = '" + search[i + 1] + "'";
        } else if (search[i] == 'adress') {
          query += "title = '" + search[i + 1] + "'";
        } else {
          query += search[i] + " = " + search[i + 1] + " AND ";
        }
      }
    }
    connection.query(query, function(err, res) {
      console.log(query);
      if (err) {
        console.log(err);
        execution.reject(err);
      }
      console.log(res);
      execution.resolve(res);
    });
    return execution.promise;

  }

  function addHouse(house) {
    var execution = q.defer();
    var query = 'INSERT INTO houses SET  title = :title, map_location =: mapLocation, location =: location, adress =: adress, offer_type =: offerType, house_type =: houseType, price =: price, rooms =: rooms, surface =: surface, date =: addedDate, description =: desc ';
    connection.query(query, {
      title: house.title,
      mapLocation: house.mapLocation,
      location: house.location,
      adress: house.adress,
      offerType: house.offerType,
      houseType: house.houseType,
      price: house.price,
      rooms: house.rooms,
      surface: house.surface,
      addedDate: house.addedDate,
      desc: house.desc
    }, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
        return;
      }
      execution.resolve(res);
    });
    return execution.promise;
  }


  function savePics(pictures) {
    var houseUrl = pictures.house + '--' + pictures.pictures;
    var execution = q.defer();
    var query = "INSERT INTO pictures SET house_id = :houseId, picture_url = :pictureUrl";
    connection.query(query, {
      houseId: pictures.house,
      pictureUrl: houseUrl
    }, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
        return;
      }
      execution.resolve(res);
    });
    return execution.promise;
  }

  function getHousePics(houseId) {
    var execution = q.defer();
    var query = "SELECT * FROM pictures WHERE house_id = :id";
    connection.query(query, {
      id: houseId
    }, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
        return;
      }
      execution.resolve(res);
    });
    return execution.promise;
  }

  function getHouses() {
    var execution = q.defer();
    var query = "SELECT * FROM houses";
    connection.query(query, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
      }
      execution.resolve(res);
    });
    return execution.promise;
  }

  function getHouse(id) {
    var execution = q.defer();
    var query = "SELECT * FROM houses WHERE id = :id";
    connection.query(query, {
      id: id
    }, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
      }
      execution.resolve(res);
    });
    return execution.promise;
  }

  function updateHouse(house) {
    var execution = q.defer();
    var query = "UPDATE houses SET map_location = :mapLocation, location = :location, offer_type = :offerType, house_type = :houseType, price = :price, rooms = :rooms, surface = :surface, date = :addedDate, description = :desc WHERE id = :id";
    connection.query(query, {
      mapLocation: house.mapLocation,
      location: house.location,
      offerType: house.offer_type,
      houseType: house.house_type,
      price: house.price,
      rooms: house.rooms,
      surface: house.surface,
      addedDate: house.addedDate,
      desc: house.description,
      id: house.id
    }, function(err, res) {
      if (err) {
        console.log(err);
        execution.reject(err);
      }
      execution.resolve(res);
    });
    return execution.promise();
  }

  function deleteHouse(houseId) {
    var execution = q.defer();
    var query = "DELETE FROM houses WHERE id = :id";
    connection.query(query, {
      id: houseId
    }, function(err, res) {
      if (err) {
        execution.reject(err);
      }
      execution.resolve(res);
    });
    return execution.promise;
  }
}

module.exports = HousingProvider();
