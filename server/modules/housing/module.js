var express = require('express');
var path = require('path');
var ManagerController = require('./controller/ManagerController');
var HousingController = require('./controller/HousingController');
var busboy = require('connect-busboy'); //middleware for form/file upload
var fs = require('fs-extra'); //File System - for file manipulation
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
})

module.exports = function(app) {
  function activateRoutes(router) {
    router.use(activateServer());
  }

  function activateServer() {
    router = express.Router();

    //////////-- GET REQUESTS --\\\\\\\\\\\\
    ////////////////////\\\\\\\\\\\\\\\\\\\\

    router.get('/api/users/:username/:password', ManagerController.authenticate);
    router.get('/api/houses', HousingController.getHouses);
    router.get('/api/houses/:id', HousingController.getHouse);
    router.get('/api/company', ManagerController.getInfo);
    router.get('/api/search/*', HousingController.search);
    router.get('/api/housePic/:houseId', function(req, res, next){
      var houseId = req.params.houseId;
      fs.readdir('img/' + houseId + '/main', function(err, file){
        console.log(file);
        res.json(file);
      })
    })
    router.get('/api/pictures/:houseId', function(req, res, next) {
      var houseId = req.params.houseId;
      fs.readdir('img/' + houseId, function(err, files) {
        delete files[files.length-1];
        console.log('Getting the house pics');
        console.log(files);
        res.json(files);
      });
    });

    //////////-- PUT REQUESTS --\\\\\\\\\\\\
    ////////////////////\\\\\\\\\\\\\\\\\\\\

    router.put('/api/houses', HousingController.updateHouse);

    //////////-- DELETE REQUESTS --\\\\\\\\\\\\
    ////////////////////\\\\\\\\\\\\\\\\\\\\

    router.delete('/api/houses/:id', HousingController.deleteHouse);
    router.delete('/api/picture/:houseId/:pictureName', function(req, res, next) {
      console.log('Picture Delete Requested by the client');
      fs.unlink('img/' + req.params.houseId + '/' + req.params.pictureName);
      res.send('OK');
    });

    //////////-- POST REQUESTS --\\\\\\\\\\\\
    ////////////////////\\\\\\\\\\\\\\\\\\\\

    router.post('/api/houses', HousingController.addHouse);
    router.post('/api/pictures', HousingController.savePics);
    router.post('/upload/:houseId/:main', function(req, res, next) {
      var houseId = req.params.houseId;
      var main = req.params.main;
      var fstream;
      req.pipe(req.busboy);
      req.busboy.on('file', function(fieldname, file, filename) {
        //Path where image will be uploaded
        if (!fs.exists('img/' + houseId) && !fs.exists('img/' + houseId + '/main')) {
          fs.mkdir('img/' + houseId, function(re){
          });
          fs.mkdir('img/' + houseId + '/main', function(re){
          });
        }
        if (main == 'true') {
          fstream = fs.createWriteStream('img/' + houseId + '/main/' + filename);
        }
        if (main == 'false') {
          console.log('This is not the main picture');
          fstream = fs.createWriteStream('img/' + houseId + '/' + filename);
        }
        file.pipe(fstream);
        fstream.on('close', function() {
          console.log("Upload Finished of " + filename);
          res.redirect('back'); //where to go next
        });
      });
    });

    return router;
  }
  return {
    activateRoutes: activateRoutes
  }
}
