(function() {
  var dependencies = [
    'ngRoute',
    'uiGmapgoogle-maps',
    'ui.bootstrap',
    'housing',
    'slick',
    'toastr',
    'angular-google-maps-geocoder'
  ];

  var module = angular.module('ezies', dependencies);
  module.config(function($routeProvider) {
    $routeProvider.otherwise({
      templateUrl: 'views/dashboard.html'
    })
  });
  module.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyBnJHD0ndP83_7TJk1cOQbipqoN8W6Wksc',
      v: '3.20', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization'
    });
  });
  module.config(['$compileProvider',
    function($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }
  ]);
}).call(this);
