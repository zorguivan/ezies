(function() {
  var module = angular.module('housing');

  module.controller('HousingController', HousingController);

  HousingController.$inject = [
    'HousingService', 'ManagerService',
    'uiGmapGoogleMapApi', '$routeParams',
    '$uibModal', 'toastr',
    '$rootScope', '$timeout',
    '$scope'
  ];

  function HousingController(
    housingService, managerService,
    uiGmapGoogleMapApi, $routeParams,
    $uibModal, toastr,
    $rootScope, $timeout, $scope) {

    toastr.options = {
      "positionClass": "your-newly-created-div-class"
    }

    var vm = this;

    vm.house = {};
    vm.files = [];
    vm.houses = [];

    vm.addHouse = addHouse;
    vm.uploadFile = uploadFile;
    vm.openSearchModal = openSearchModal;
    vm.openDeleteModal = openDeleteModal;
    vm.adminSearchModal = adminSearchModal;
    vm.updateHouse = updateHouse;
    vm.deleteHouse = deleteHouse;
    vm.lookingEye = lookingEye;
    vm.checkAdress = checkAdress;

    refresh();


    function refresh() {
      if ($rootScope.globalState) {
        vm.globalState = $rootScope.GlobalState;
        console.log($rootScope);
      }
      if (!$routeParams.id) {
        housingService.getHouses().then(function(houses) {
          vm.houses = houses;
          vm.houses.pictures = {};
          // console.log(houses);
          toastr.success('Houses has been loaded');
          vm.houses.forEach(function(house) {
            housingService.getHousePicture(house.id).then(function(picture) {
              house.pictures = picture;
              console.log(house);
            });
          })
        });

      } else {
        housingService.getHouse($routeParams.id).then(function(house) {
          vm.house = house;
          vm.house.pictures = {};
          toastr.info('Loading House and initialising map location');
          toastr.success('House has been loaded');
          var mapLocation = house.map_location.split("/");
          vm.location = {
            id: mapLocation[0],
            coords: {
              latitude: mapLocation[1],
              longitude: mapLocation[2]
            }
          };
          housingService.getHousePicture(house.id).then(function(picture) {
            vm.house.pictures[0] = picture;
            console.log('Loading the main picture...');
            console.log(picture);
            housingService.getHousePictures(house.id).then(function(pictures) {
              vm.house.pictures[1] = pictures;
              console.log('Loading the categeory pictures...');
              // console.log(pictures);
            })
          });
          // $('.slider-for').slick({
          //   slidesToShow: 1,
          //   slidesToScroll: 1,
          //   arrows: false,
          //   fade: true,
          //   asNavFor: '.slider'
          // });
          // $('.slider').slick({
          //   slidesToShow: 3,
          //   slidesToScroll: 1,
          //   asNavFor: '.slider-for',
          //   dots: true,
          //   centerMode: true,
          //   focusOnSelect: true
          // });
          console.log(vm.house);
        });
      }
    }

    vm.map = {
      center: {
        latitude: 35.8192073,
        longitude: 10.6056673
      },
      zoom: 8,
      markers: []
    }

    function lookingEye(location) {
      vm.house.adress = undefined;
      housingService.getLocation(location).then(function(newLocation) {
        var marker = {
          id: Date.now(),
          coords: {
            latitude: newLocation.geometry.location.lat,
            longitude: newLocation.geometry.location.lng
          }
        }
        vm.map.center.latitude = newLocation.geometry.location.lat;
        vm.map.center.longitude = newLocation.geometry.location.lng;
        vm.map.zoom = 11;
        vm.map.markers[0] = marker;
      })
    }
    function checkAdress(location ,adress){
      if(location == "Tunis center") location = "tunis";
      housingService.checkAdress(location ,adress).then(function(newAdress){
        console.log(location);
        console.log(adress);
        if(newAdress && newAdress.length != 0){
          toastr.success('Adress found, Changing location.');
          var marker = {
            id: Date.now(),
            coords: {
              latitude: newAdress.geometry.location.lat,
              longitude: newAdress.geometry.location.lng
            }
          }
          vm.map.center.latitude = newAdress.geometry.location.lat;
          vm.map.center.longitude = newAdress.geometry.location.lng;
          vm.map.zoom = 11;
          vm.map.markers[0] = marker;
        } else {

          toastr.warning('Adress Not found! Search again.');
        }
      })
    }

    function updateHouse(houseId) {
      housingService.updateHouse(houseId).then(function(res) {
        toastr.info('House has been updated');
      });
    }

    function deleteHouse(houseId) {
      housingService.deleteHouse(houseId).then(function(res) {
        toastr.warning('House has been deleted');

        refresh();
      })
    }

    function openDeleteModal(houseId, pictureName) {
      var instance = $uibModal.open({
        templateUrl: '/modules/housing/template/confirmation-modal.html'
      });
      instance.result.then(function() {
        housingService.deletePicture(houseId, pictureName).then(function(res) {
          toastr.success('Piture has been deleted');
          refresh();
        })
      });
    }

    function openSearchModal() {
      var instance = $uibModal.open({
        templateUrl: "/modules/housing/template/search-modal.html"
      });
      instance.result.then(function(search) {
        refresh();
        if (search) {
          console.log(search);
          var query = '';
          for (var prop in search) {
            if (search.hasOwnProperty(prop)) {
              query = query + '/' + prop + '/' + search[prop];
            }
          }
          // console.log(query);
          toastr.info('Searching...');
          housingService.search(query).then(function(houses) {
            toastr.success('Searching Done');
            console.log(houses);
            vm.houses = houses;
          })
        }
      });
    }

    function adminSearchModal() {
      var instance = $uibModal.open({
        templateUrl: "/modules/housing/template/admin-search-modal.html"
      });

      instance.result.then(function(search) {
        refresh();
        if (search) {
          console.log(search);
          var query = '';
          for (var prop in search) {
            if (search.hasOwnProperty(prop)) {
              query = query + '/' + prop + '/' + search[prop];
            }
          }
          // console.log(query);
          toastr.info('Searching...');
          housingService.search(query).then(function(houses) {
            toastr.success('Searching Done');
            vm.houses = houses;
          })
        }
      });
    }

    function uploadFile(houseId) {
      // console.log(vm.files);
      var files = vm.files;
      var uploadUrl = "/fileUpload";
      housingService.uploadFileToUrl(houseId, files.main[0], true).then(function(res) {
        for (var i = 0; i < files.category.length; i++) {
          housingService.uploadFileToUrl(houseId, files.category[i], false).then(function(res) {
            console.log(res);
          });
        }
      });
    };


    function addHouse(house) {
      console.log(house.title);
      if (vm.map.markers[0]) {
        house.mapLocation = vm.map.markers[0].id + '/' + vm.map.markers[0].coords.latitude + '/' + vm.map.markers[0].coords.longitude;
      }

      function formattedDate(date) {
        var d = new Date(date || Date.now()),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
      }

      house.addedDate = formattedDate();
      housingService.addHouse(house).then(function(house) {
        uploadFile(house.insertId);
        toastr.success('House and Pictures has been saved');
      });
      // console.log(house);
    }
  }
}).call(this);
