(function() {
  var module = angular.module('housing');
  module.controller('ManagerController', ManagerController);

  ManagerController.$inject = ['ManagerService', '$rootScope', 'toastr'];

  function ManagerController(ManagerService, $rootScope, toastr) {


    var vm = this;
    vm.user = {};
    vm.comapany = {};
    vm.loginError = false
    refresh();

    function refresh() {}
    ManagerService.getInfo().then(function(res) {
      vm.comapany = res;
    })
    vm.map = {
      center: {
        latitude: 35.8597494,
        longitude: 10.5995927
      },
      zoom: 8,
      coords: {
        latitude: 35.8666701,
        longitude: 10.6092198
      },
      id: 1462493908169
    };

    vm.authenticate = authenticate;

    function authenticate(user) {
      ManagerService.authenticate(user).then(function(res) {
        console.log(res);
        if (res.id) {
          $rootScope.GlobalState = true;
          toastr.info('Redirecting Location ...');
          window.location = "#/insert";

        } else {
          vm.loginError = true;
          toastr.error('Wrong password or username, Please Try again');

        }
      })
    }
  }
}).call(this);
