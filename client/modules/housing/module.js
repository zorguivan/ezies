(function() {
  var module = angular.module('housing', []);


  module.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/manager', {
      resolve: {
        "check": function($location, $rootScope) {
          if ($rootScope.GlobalState) {
            $location.path('/insert');
          }
        }
      },
      templateUrl: '/modules/housing/views/login.html',
      controller: 'ManagerController',
      controllerAs: 'mc'
    });
    $routeProvider.when('/insert', {
      resolve: {
        "check": function($location, $rootScope) {
          if (!$rootScope.GlobalState) {
            $location.path('/manager');
          }
        }
      },
      templateUrl: '/modules/housing/views/insert.html',
      controller: 'HousingController',
      controllerAs: 'hc'

    });
    $routeProvider.when('/logout', {
      resolve: {
        "check": function($location, $rootScope){
          if ($rootScope.GlobalState) {
            $rootScope.GlobalState = false ;
            $location.path('/');
          }
        }
      }
    });
    $routeProvider.when('/house/:id', {
      templateUrl: '/modules/housing/views/house.html',
      controller: 'HousingController',
      controllerAs: 'hc'
    });
    $routeProvider.when('/contact', {
      templateUrl: '/modules/housing/views/contact.html',
      controller: 'ManagerController',
      controllerAs: 'mc'
    })
    $routeProvider.when('/house-edit/:id', {
      templateUrl: '/modules/housing/views/house-edit.html',
      controller: 'HousingController',
      controllerAs: 'hc'
    });
    $routeProvider.when('/houses-sale', {
      templateUrl: '/modules/housing/views/houses-sale.html',
      controller: 'HousingController',
      controllerAs : 'hc'
    });
    $routeProvider.when('/houses-rent', {
      templateUrl: '/modules/housing/views/houses-rent.html',
      controller: 'HousingController',
      controllerAs: 'hc'
    });
    $routeProvider.when('/all-houses', {
      resolve: {
        "check": function($location, $rootScope) {
          if (!$rootScope.GlobalState) {
            $location.path('/manager');
          }
        }
      },
      templateUrl: '/modules/housing/views/all-houses.html',
      controller: 'HousingController',
      controllerAs: 'hc'

    });
    // $routeProvider.when('/search' {
    //   templateUrl : '/modules/housing/views/search.html'
    //   controller: 'HousingController',
    //   controllerAs: 'hc'
    // })
  }])
}).call(this);
