(function() {
  var module = angular.module('housing');

  module.service('ManagerService', ManagerService);

  ManagerService.$inject = ['$q', '$http'];

  function ManagerService($q, $http) {

    return {
      authenticate: authenticate,
      getInfo: getInfo
    }

    function authenticate(user) {
      var execution = $q.defer();

      $http.get('/api/users/' + user.username + '/' + user.password).then(function(res) {
        // console.log(res.session);
        execution.resolve(res.data);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject();
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject()
        } else {
          console.log(response.message);
          execution.reject()
        }
      });
      return execution.promise;
    }

    function getInfo() {
      var execution = $q.defer();
      $http.get('/api/company').then(function(res) {
        var info = res.data[0];
        execution.resolve(info);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject();
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject()
        } else {
          console.log(response.message);
          execution.reject()
        }
      });
      return execution.promise;
    }
  }
}).call(this);
