(function() {
  var module = angular.module('housing');

  module.service('HousingService', HousingService);
  HousingService.$inject = ['$q', '$http', 'toastr'];

  function HousingService($q, $http, toastr) {



    return {
      getHouses: getHouses,
      getHouse: getHouse,
      addHouse: addHouse,
      savePics: savePics,
      search: search,
      uploadFileToUrl: uploadFileToUrl,
      getHousePictures: getHousePictures,
      updateHouse: updateHouse,
      deleteHouse: deleteHouse,
      deletePicture: deletePicture,
      getHousePicture: getHousePicture,
      getLocation: getLocation,
      checkAdress: checkAdress
    }

    function getHouses() {
      var execution = $q.defer();
      $http.get('/api/houses').then(function(res) {

        var houses = res.data;
        execution.resolve(houses);
      }).catch(function(response) {
        toastr.error('Error loading the houses please try again later..! ');
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function getHousePicture(houseId) {
      var execution = $q.defer();
      $http.get('/api/housePic/' + houseId).then(function(res) {
        console.log('Picture Request');
        console.log(res.data);
        var picture = res.data;
        execution.resolve(picture);
      }).catch(function(response) {
        toastr.error('Error loading the house picture, please try again later..! ');
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function getLocation(location){
      var execution = $q.defer();
      $http.get('http://maps.googleapis.com/maps/api/geocode/json?address=' + location)
      .then(function(res){
        execution.resolve(res.data.results[0]);
      }).catch(function(err){
        console.log(err);
        execution.reject(err);
      });
      return execution.promise;
    }
    function checkAdress(location, adress){
      var execution = $q.defer();
      $http.get('http://maps.googleapis.com/maps/api/geocode/json?address=' + adress + ',' + location)
      .then(function(res){
        console.log('http://maps.googleapis.com/maps/api/geocode/json?address=' + adress + ',' + location);
        execution.resolve(res.data.results[0]);
      }).catch(function(err){
        console.log(err);
        execution.reject(err);
      });
      return execution.promise;
    }
    function uploadFileToUrl(houseId, file, main) {
      var execution = $q.defer();

      var fd = new FormData();
      fd.append('file', file);
      $http.post('/upload/' + houseId + '/' + main, fd, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).then(function(res) {
        execution.resolve(res);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function updateHouse(house) {
      var execution = $q.defer();
      $http.put('/api/houses', house).then(function(res) {
        execution.resolve(res);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function getHouse(house) {
      var execution = $q.defer();
      $http.get('/api/houses/' + house).then(function(res) {
        var house = res.data[0];
        execution.resolve(house);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function search(search) {
      // console.log(search);
      var execution = $q.defer();
      console.log(search);
      $http.get('/api/search' + search).then(function(res) {
        var houses = res.data;
        execution.resolve(houses);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function deleteHouse(houseId) {
      var execution = $q.defer();
      $http.delete('/api/houses/' + houseId).then(function(res) {
        execution.resolve();
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function deletePicture(houseId, pictureName) {
      var execution = $q.defer();
      $http.delete('/api/picture/' + houseId + '/' + pictureName).then(function(res) {
        execution.resolve();
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function addHouse(house) {
      var execution = $q.defer();
      console.log(house);
      $http.post('/api/houses', house).then(function(res) {
        console.log(res.data);
        var insertedHouse = res.data;
        execution.resolve(insertedHouse);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function getHousePictures(houseId) {
      var execution = $q.defer();
      $http.get('/api/pictures/' + houseId).then(function(res) {
        var pictures = res.data;
        execution.resolve(pictures);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }

    function savePics(houseId, pictures) {
      console.log('Called');
      var execution = $q.defer();
      $http.post('/api/pictures', {
        house: houseId,
        pictures: pictures
      }).then(function(res) {
        execution.resolve(res);
      }).catch(function(response) {
        if (response.status == 404) {
          console.log(response.message);
          execution.reject(response.message);
        } else if (response.status > 399 && response.status < 500) {
          console.log(response.message);
          execution.reject(response.message)
        } else {
          console.log(response.message);
          execution.reject(response.message);
        }
      });
      return execution.promise;
    }
  }
}).call(this);
