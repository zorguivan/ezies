(function() {
  var module = angular.module('housing');

  module.directive('fileModel', ['$parse', function($parse) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var isMultiple = attrs.multiple;
        var modelSetter = model.assign;

        element.bind('change', function() {
          scope.$apply(function() {
            //             modelSetter(scope, values);
            //           } else {
            //             modelSetter(scope, values[0]);
            angular.forEach(element[0].files, function(item) {
              var blob = new Blob([ item ], { type : 'text/plain' });
              item.url = URL.createObjectURL(item);
            });
            modelSetter(scope, element[0].files);
            console.log(element[0].files);
          });
        });
      }
    };
  }]);
}).call(this);
  // directive('ngFileModel', ngFileModel);
  //
  // ngFileModel.$inject = ['$parse'];
  //
  // function ngFileModel($parse) {
  //
  //   return {
  //     restrict: 'A',
  //     link: function(scope, element, attrs) {
  //       var model = $parse(attrs.ngFileModel);
  //       var isMultiple = attrs.multiple;
  //       var modelSetter = model.assign;
  //       element.bind('change', function() {
  //         var values = [];
  //         angular.forEach(element[0].files, function(item) {
  //           var value = {
  //             // File Name
  //             name: item.name,
  //             //File Size
  //             size: item.size,
  //             //File URL to view
  //             url: URL.createObjectURL(item),
  //             // File Input Value
  //             _file: item
  //           };
  //           values.push(value);
  //         });
  //         scope.$apply(function() {
  //           if (isMultiple) {
  //             modelSetter(scope, values);
  //           } else {
  //             modelSetter(scope, values[0]);
  //           }
  //         });
  //       });
  //     }
  //   };
  // }
