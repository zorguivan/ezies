var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var nunjucks = require('nunjucks');
var mysql = require('mysql');
var busboy = require('connect-busboy'); //middleware for form/file upload
var fs = require('fs-extra'); //File System - for file manipulation

var app = express();
app.use(require('morgan')('dev'));
var session = require('express-session');
var FileStore = require('session-file-store')(session);

app.use(busboy());
// console.log(__dirname);


var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "ezies"
});

connection.config.queryFormat = function(query, values) {
  if (!values) return query;
  return query.replace(/\:(\w+)/g, function(txt, key) {
    if (values.hasOwnProperty(key)) {
      return this.escape(values[key]);
    }
    return txt;
  }.bind(this));
};

// view engine setup
nunjucks.configure('client', {
  autoescape: true,
  express: app
})


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'img')));

app.use(session({
  name: 'server-session-cookie-id',
  secret: 'my express secret',
  saveUninitialized: true,
  resave: true,
  store: new FileStore()
}));

//
// app.use(function printSession(req, res, next){
//   console.log(req.session);
//   next();
// });


var housing = require('./server/modules/housing/module.js')(app);

var ManagerProvider = require('./server/modules/housing/provider/ManagerProvider');
var HousingProvider = require('./server/modules/housing/provider/HousingProvider');


ManagerProvider.setConnection(connection);
HousingProvider.setConnection(connection);

var router = express.Router();
housing.activateRoutes(router);
app.use(router);
// Loading index.html as main main file

// app.get('/', function(req, res, next) {
//   var sess = req.session;
//   console.log('-------------------------------------------------');
//   console.log(sess);
// });


// app.use(session({
//   key: 'session_cookie_name',
//   secret: 'session_cookie_secret',
//   store: sessionStore,
//   resave: true,
//   saveUninitialized: true
// }));
// app.use(function(req, res, next) {
//   var sess = req.session;
//   console.log('<><><><><><><><><><><><><><><><><><<><<><><><');
//   console.log(sess);
// })
//
// app.route('/upload').post(function(req, res, next) {
//
//
// });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {

    res.status(err.status || 500);
    res.send('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
