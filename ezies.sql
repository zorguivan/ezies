-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2016 at 05:35 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ezies`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `phone_number` int(8) NOT NULL,
  `email` varchar(30) NOT NULL,
  `facebook` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `phone_number`, `email`, `facebook`) VALUES
(1, 'The Renting Agency', 54470096, 'totally.random@random.com', 'http://www.facebook.com');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE IF NOT EXISTS `houses` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `map_location` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `location` varchar(20) NOT NULL,
  `adress` varchar(200) NOT NULL,
  `offer_type` varchar(10) NOT NULL,
  `house_type` varchar(10) NOT NULL,
  `price` varchar(6) NOT NULL,
  `rooms` int(10) NOT NULL,
  `surface` int(6) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`id`, `map_location`, `title`, `location`, `adress`, `offer_type`, `house_type`, `price`, `rooms`, `surface`, `date`, `description`) VALUES
(1, '1469400485589/35.47856499535729/10.56610107421875', '', 'Tunis', '', 'sale', 'Appertment', '450', 5, 450, '2016-07-25', 'This is a testing House'),
(2, '1469400518657/35.721987809328716/10.50430297851562', '', 'Ariana', '', 'rent', 'Villa', '230', 2, 230, '2016-07-25', 'This is a testing House N2'),
(3, '1469403718845/35.74205383068035/10.52764892578125', '', 'Tunis', '', 'sale', 'Appertment', '450', 4, 450, '2016-07-25', 'This is a stupid other test but it works :D :D :D'),
(4, '1469404584120/35.63051198300061/10.17059326171875', '', 'Tunis', '', 'sale', 'Appertment', '560', 5, 50, '2016-07-25', 'Something Somewhere');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'zorguivan', 'zorgui.oz@gmail.com', 'Chaostheory');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
